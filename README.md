# MayaPy-environment

It is a clone of Python folder in Maya 2018 for Windows.  
You can copy this repository to Maya's Python folder directly.  
The default installation path is: **C:\Program Files\Autodesk\Maya2018\Python**  


## List of custom wheels

These are core but non-pure wheels, so I personally built them by myself.

- **Cython**-0.29  
- **numpy**-1.14.5 (built with MKL 2018, dynamic DLL)  
- **scipy**-1.1.0 (built with MKL 2018, static)  
- **h5py**-2.8.0 (built on numpy)  
- **tensorflow**-1.11.0 (built with MKL 2018 and CUDA 9.0/cuDNN 7.0, ComputeCapability 5.0,6.1)  

If you want to change/update these libraries, you may need to build these by yourself.  
To run **pip** to update these, you need to activate **cmd** with **admin**, because Maya is installed under **C:\Program Files**.  
The following command will be helpful to install/update new python packages.
```sh
> path=%PATH%;C:\Program Files\Autodesk\Maya2018\bin;C:\Program Files\Autodesk\Maya2018\Python\Scripts
```


## Reproduce the installation process

You can see the commit history.
